# React node api viewer

> API Viewer for your api-docker documentation. [api-docker](https://gitlab.com/siddhesh.kulkarni/node-api-docker "API Documentation generator") 

## Demo
-  [GlobalAuth Documentation](https://globalauth.herokuapp.com/docs/ "GlobalAuth Documentation")

## Usage

- Step 1: Clone/Download this repository.
- Step 2: Put `/docs/` folder from the repo into your api's `/public` folder.
- Step 3: Launch your API and navigate to `/docs/` path.